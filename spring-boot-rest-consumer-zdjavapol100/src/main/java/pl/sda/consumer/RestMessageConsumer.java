package pl.sda.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.sda.model.Message;
import pl.sda.model.PartialMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class RestMessageConsumer implements CommandLineRunner {

    //Zmiana sposobu twrozenia obiektu RestTemplate w celu poprawnej obsługi zapytań HTTP PATCH
    //Szczegóły https://stackoverflow.com/questions/41368762/resttemplate-and-patch-invalid/52663667
    private RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

    private ObjectMapper objectMapper = new ObjectMapper();

    private void invokeRestGetAllMessages() throws JsonProcessingException {
        final String getAllUrl = "http://localhost:8080/api/messages";
        final String json = restTemplate.getForObject(getAllUrl, String.class);

        log.info("Received JSON with all messages: " + json);

        final List<Message> messages =
                objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, Message.class));

        messages.forEach(m -> log.info(m.toString()));
    }

    private void invokeRestMessageById(Integer id) {
        final String getMessageUrl = "http://localhost:8080/api/messages/{identifier}";

        Map<String, Integer> params = new HashMap<>();
        params.put("identifier", id);

        Message message = restTemplate.getForObject(getMessageUrl, Message.class, params);
        log.info("Received message " + message);
    }

    private void invokeRestPostMessage() {
        final String postMessageUrl = "http://localhost:8080/api/messages/";
        Message newMessage = new Message(3, "new message 3", "blocker");

        restTemplate.postForObject(postMessageUrl, newMessage, Message.class);
        log.info("Added message: " + newMessage);
    }

    private void invokeRestDeleteMessageById(Integer id) {
        final String deleteMessageUrl = "http://localhost:8080/api/messages/{identifier}";

        Map<String, Integer> params = new HashMap<>();
        params.put("identifier", id);

        restTemplate.delete(deleteMessageUrl, params);
        log.info("Deleted message with id: " + id);
    }

    private void invokeRestPatchMessage() {
        final String patchMessageUrl = "http://localhost:8080/api/messages/";
        PartialMessage partialMessage = new PartialMessage(3, "low");

        restTemplate.patchForObject(patchMessageUrl, partialMessage, PartialMessage.class);
        log.info("Updated partial message: " + partialMessage);
    }

    @Override
    public void run(String... args) throws Exception {
        invokeRestPostMessage();
        invokeRestGetAllMessages();
        invokeRestMessageById(2);
        invokeRestDeleteMessageById(1);
        invokeRestGetAllMessages();

        invokeRestPatchMessage();
        invokeRestGetAllMessages();
    }
}
