package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestConsumerZdjavapol100Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestConsumerZdjavapol100Application.class, args);
	}

}
