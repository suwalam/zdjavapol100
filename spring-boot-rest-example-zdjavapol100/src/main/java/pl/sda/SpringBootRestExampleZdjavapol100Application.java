package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestExampleZdjavapol100Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestExampleZdjavapol100Application.class, args);
	}
}
