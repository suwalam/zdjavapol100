package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Message;
import pl.sda.model.PartialMessage;
import pl.sda.service.MessageService;

import java.util.List;

@Slf4j
@RestController //@Controller + @ResponseBody
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/api/messages", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getAllMessages() {
        log.info("Get all messages requested");
        return messageService.getAllMessages();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/api/messages")
    public void createMessage(@RequestBody Message message) {
        log.info("Received message to save: " + message);
        messageService.addMessage(message);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/api/messages/{identifier}")
    public Message getMessageById(@PathVariable Integer identifier) {
        log.info("Received message identifier: " + identifier);
        return messageService.getMessageById(identifier);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(path = "/api/messages")
    public void updateMessage(@RequestBody Message message) {
        log.info("Received message to update: " + message);
        messageService.updateMessage(message);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path = "/api/messages/{identifier}")
    public void deleteMessageById(@PathVariable Integer identifier) {
        log.info("Received message identifier to delete: " + identifier);
        messageService.deleteMessage(identifier);
    }

    //Update only importance Message field
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(path = "/api/messages")
    public void updatePartialMessage(@RequestBody PartialMessage partialMessage) {
        log.info("Received partial message to update: " + partialMessage);
        messageService.updatePartialMessage(partialMessage);
    }
}
