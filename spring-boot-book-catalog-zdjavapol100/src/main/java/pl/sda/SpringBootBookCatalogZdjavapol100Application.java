package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBookCatalogZdjavapol100Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBookCatalogZdjavapol100Application.class, args);
	}

}
