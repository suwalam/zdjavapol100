package pl.sda.cyclicdependency;


import org.springframework.stereotype.Component;

@Component
public class ClassBThatRequiresA {

    private ClassCommon classCommon;

    public ClassBThatRequiresA(ClassCommon classCommon) {
        this.classCommon = classCommon;
    }
}
