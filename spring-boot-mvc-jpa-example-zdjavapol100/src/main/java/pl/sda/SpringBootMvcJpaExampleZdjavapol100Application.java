package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMvcJpaExampleZdjavapol100Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMvcJpaExampleZdjavapol100Application.class, args);
	}

}
