package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Slf4j
@Controller //adnotacja służąca do zakomunikowania Springowi, że klasa obsługuje żądania HTTP
public class HelloController {

    @GetMapping("/hello")
    public String hello() {
        return "welcome"; //nazwa pliku o rozszerzeniu .html
    }

    @GetMapping("/hello-msg")
    public String helloMessage(Model model) { //spring dostarcza automatycznie obiekt Model
        //atrybut do odczytania w pliku HTML za pomocą składni <p th:text="${helloMsg}"></p>
        model.addAttribute("helloMsg", "Hello from backend");

        model.addAttribute("elements", Arrays.asList("elem1", "elem2", "elem3"));

        log.info("wyslano strone welcome-msg.html");

        return "welcome-msg";
    }

}
